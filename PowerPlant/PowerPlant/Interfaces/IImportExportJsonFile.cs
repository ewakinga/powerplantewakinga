﻿using PowerPlant.JsonFile;

namespace PowerPlant.Interfaces
{
    public interface IImportExportJsonFile
    {
        void ExportDataToFile(SettingsFileData settingsFile);
        SettingsFileData GetDataFromFile();
    }
}