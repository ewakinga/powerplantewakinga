﻿namespace PowerPlant.Interfaces
{
    public interface IPowerPlantService
    {
        PowerPlantDataProvider.PowerPlant GetPowerPlant();
    }
}