﻿using PowerPlant.Interfaces;
using PowerPlant.JsonFile;
using PowerPlantDataProvider;

namespace PowerPlant.Services
{
   public class EnergyCalculator : IEnergyCalculator
   {
        private static double _sum;

        public void CalculateEnergySum(double currentEnergy, int intervalTime)
        {
            _sum += currentEnergy * intervalTime / (1000 * 60 * 60);
        }
       public double GetSumOfEnergy()
       {
           return _sum;
       }
       public void ClearSumOfEnergy()
       {
           _sum = 0;
       }
    }
}
