﻿using System.Collections.Generic;
using JsonHelper;
using PowerPlant.Dtos;
using PowerPlantDataLayer.Models;

namespace PowerPlant.Mappers
{
   public class EntityToDtoMapper
    {
        public static UserDto UserEntityModelToDto(User user)
        {
            if (user == null)
            {
                return null;
            }

            var userDto = new UserDto
            {
                Id = user.Id,
                Login = user.Login,
                Password = user.Password,
                IsAdmin = user.IsAdmin
            };

            return userDto;
        }
        public static AlertOfCrossingDto AlertsEntityModelToDto(AlertOfCrossing alert)
        {
            if (alert == null)
            {
                return null;
            }

            var alertDto = new AlertOfCrossingDto
            {
                Id = alert.Id,
                DeviceName = alert.DeviceName,
                ParameterName = alert.ParameterName,
                TimeOfOccurrence = alert.TimeOfOccurrence,
                LoggedUser = alert.LoggedUser

            };
            return alertDto;
        }

        public static List<AlertOfCrossingDto> AlertsListEntityModelToDto(List<AlertOfCrossing> alertList)
        {
            List<AlertOfCrossingDto> alertOfCrossingList=new List<AlertOfCrossingDto>();
            foreach (var alert in alertList)
            {
                var alertDto=new AlertOfCrossingDto();
                alertDto.Id = alert.Id;
                alertDto.DeviceName = alert.DeviceName;
                alertDto.LoggedUser = alert.LoggedUser;
                alertDto.ParameterName = alert.ParameterName;
                alertDto.TimeOfOccurrence = alert.TimeOfOccurrence;
                alertOfCrossingList.Add(alertDto);
            }
            return alertOfCrossingList;

        }
     
    }
}
