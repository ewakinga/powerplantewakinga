﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JsonHelper;
using PowerPlant.Dtos;
using PowerPlantDataLayer.Models;

namespace PowerPlant.Mappers
{
   public class DtoToEntityMapper
    {
        public static User UserDtoToEntityModel(UserDto userDto)
        {
            if (userDto == null)
            {
                return null;
            }

            var user = new User
            {
                Id = userDto.Id,
                Login = userDto.Login,
                Password = userDto.Password,
                IsAdmin = userDto.IsAdmin
            };

            return user;
        }

        public static AlertOfCrossing AlertOfCrossingDtoToEntityModel(AlertOfCrossingDto alertDto)
        {
            if (alertDto == null)
            {
                return null;
            }
            var alert = new AlertOfCrossing
            {
                Id = alertDto.Id,
                DeviceName = alertDto.DeviceName,
                ParameterName = alertDto.ParameterName,
                TimeOfOccurrence = alertDto.TimeOfOccurrence,
                LoggedUser = alertDto.LoggedUser
            };
            return alert;
        }
    }
}
