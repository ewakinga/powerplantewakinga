﻿using System;
using System.Collections.Generic;
using System.IO;
using JsonHelper;
using PowerPlant.Interfaces;

namespace PowerPlant.JsonFile
{
    public class ImportExportJsonFile : IImportExportJsonFile
    {   
        public static SettingsFileData SettingsFileData;

        public void ExportDataToFile(SettingsFileData settingsFile)
        {
            var fileRepository = new FileRepository<SettingsFileData>(new JsonHelper.JsonMapper());
            fileRepository.Save("JsonSettings.json", settingsFile);

        }

        public SettingsFileData GetDataFromFile()
        {
            var jsonMapper=new JsonMapper();
            SettingsFileData settingsFile;
            string filepath = "JsonSettings.json";
            if (File.Exists(filepath))
            {
               settingsFile = jsonMapper.ToContent<SettingsFileData>(filepath);
               SettingsFileData = settingsFile;
            }
            else
            {
                settingsFile = null;
            }
            return settingsFile;
        }
    }
}
