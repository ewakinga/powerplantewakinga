﻿using System.IO;
using Newtonsoft.Json;

namespace PowerPlant.JsonFile
{
   public class JsonMapper
    {
        public T ToContent<T>(string filepath)
        {
            return JsonConvert.DeserializeObject<T>(File.ReadAllText(filepath));         
        }
        public string FromContent<T>(T content)
        {
            return JsonConvert.SerializeObject(content, Formatting.Indented);
        }
    }
}
