﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PowerPlant.Interfaces;

namespace PowerPlant.JsonFile
{
   public class GetSettingsDataFromJsonFile : IGetSettingsDataFromJsonFile
   {
        public int GetRefreshingTime()
        {
            int refreshingTime = 0;
            if (ImportExportJsonFile.SettingsFileData != null)
            {
                refreshingTime = ImportExportJsonFile.SettingsFileData.RefreshingTime;
            }
            else
            {
                refreshingTime = 250;
            }
            return refreshingTime;
        }

        public double GetMinValue(double typicalValue)
        {
            var minValue=0.0;
                   
            if (ImportExportJsonFile.SettingsFileData!=null)
            {
                minValue = typicalValue - (ImportExportJsonFile.SettingsFileData.AcceptableDeviation * typicalValue / 100);
            }
            return minValue;
        }
        public double GetMaxValue(double typicalValue)
        {
            double maxValue = 0;
            if (ImportExportJsonFile.SettingsFileData != null)
            {
                maxValue = typicalValue + (ImportExportJsonFile.SettingsFileData.AcceptableDeviation * typicalValue / 100);
            }
            return maxValue;
        }
    }
}
