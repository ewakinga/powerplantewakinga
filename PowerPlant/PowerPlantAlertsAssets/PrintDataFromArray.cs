﻿using System;
using System.Collections.Generic;
using System.Linq;
using JsonHelper;

namespace _PowerPlantAlertsAssets
{
    public class PrintDataFromArray
    {
        private readonly Dictionary<int, int> _dictionaryWithNumberOfAlerts = new Dictionary<int, int>();
        private readonly List<DateTime> _listOfDates = new List<DateTime>();
        public void PrintData(AlertOfCrossingDto[] alertArray)
        {
            var fromPeriod = alertArray.Select(a => a.TimeOfOccurrence).Min();
            var toPeriod = alertArray.Select(a => a.TimeOfOccurrence).Max();

            Console.WriteLine("Data range: " + fromPeriod + " - " + toPeriod);

            foreach (var alert in alertArray)
            {
                var data = new DateTime(alert.TimeOfOccurrence.Year, alert.TimeOfOccurrence.Month, alert.TimeOfOccurrence.Day);
                if (!_listOfDates.Contains(data))
                {
                    _listOfDates.Add(data);
                }
            }
            foreach (var date in _listOfDates)
            {           
                Console.Write("Day: " + GetEditDate(date) + ":\n");

                foreach (var alert in alertArray)
                {
                    if (alert.TimeOfOccurrence.Date == date)
                    {
                        if (!_dictionaryWithNumberOfAlerts.ContainsKey(alert.TimeOfOccurrence.Hour))
                        {
                            _dictionaryWithNumberOfAlerts.Add(alert.TimeOfOccurrence.Hour, 1);
                        }
                        else
                        {
                            _dictionaryWithNumberOfAlerts[alert.TimeOfOccurrence.Hour] += 1;
                        }
                    }
                }
                foreach (var divace in _dictionaryWithNumberOfAlerts)
                {
                    Console.WriteLine(" Hour: " + divace.Key + ":00" + " to "+divace.Key+ ":59 alerts number: " + divace.Value);
                }
            }
        }
        public string GetEditDate(DateTime date)
        {
            return date.ToString("dd-MM-yy");            
        }
    }
}
