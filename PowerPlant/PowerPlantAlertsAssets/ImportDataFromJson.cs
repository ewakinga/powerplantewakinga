﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JsonHelper;

namespace _PowerPlantAlertsAssets
{
   public class ImportDataFromJson
    {
        public AlertOfCrossingDto[] GetData()
        {
            Console.WriteLine("File name:");
            string name = Console.ReadLine();
            if (!name.EndsWith(".json"))
            {
                name = name + ".json";
            }

            string filepath = @"..\..\..\PowerPlandCLI\bin\Debug\" + name;
            if (!File.Exists(filepath))
            {
                Console.WriteLine("File doesn't exist.");
                return null;
            }
            var jsonMapper=new JsonMapper();
            var assertList = jsonMapper.ToContent<AlertOfCrossingDto[]>(filepath);

            return assertList;
        }
    }
}
