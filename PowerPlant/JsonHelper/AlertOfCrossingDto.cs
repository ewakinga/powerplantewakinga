﻿using System;

namespace JsonHelper
{
   public class AlertOfCrossingDto
   {
       public int Id;
       public string DeviceName;
       public string ParameterName;
       public DateTime TimeOfOccurrence;
       public string LoggedUser;
   }
}
