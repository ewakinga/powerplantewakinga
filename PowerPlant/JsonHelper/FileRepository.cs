﻿using System.IO;

namespace JsonHelper
{
    public class FileRepository<T>
    {
        private JsonMapper jsonMapper;

        public FileRepository(JsonMapper jsonMapper)
        {
            this.jsonMapper = jsonMapper;
        }
        public void Save(string filePath, T content)
        {
            File.WriteAllText(filePath, jsonMapper.FromContent(content));
        }
    }
}
