﻿using System;
using System.IO;
using System.Xml;
using Newtonsoft.Json;

namespace JsonHelper
{
   public class JsonMapper
    {
        public T ToContent<T>(string filepath)
        {
         return JsonConvert.DeserializeObject<T>(File.ReadAllText(filepath));
        }
        public string FromContent<T>(T content)
        {
            return JsonConvert.SerializeObject(content, Newtonsoft.Json.Formatting.Indented);
        }
    }
}
