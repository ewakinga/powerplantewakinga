﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using JsonHelper;

namespace _PowerPlantAlertsAssets
{
    public class PrintDataFromArray
    {
        private readonly Dictionary<int, int> _dictionaryWithNumberOfAlerts = new Dictionary<int, int>();
        private readonly List<DateTime> _listOfDates = new List<DateTime>();
        public void PrintData(AlertOfCrossingDto[] alertArray)
        {
            
            var fromPeriod = alertArray.Select(a => a.TimeOfOccurrence).Min();
            var toPeriod = alertArray.Select(a => a.TimeOfOccurrence).Max();
            Console.WriteLine("Zakres pobranych danych: " + fromPeriod + "-" + toPeriod);



            foreach (var alert in alertArray)
            {
                var data = new DateTime(alert.TimeOfOccurrence.Year, alert.TimeOfOccurrence.Month, alert.TimeOfOccurrence.Day);
                if (!_listOfDates.Contains(data))
                {
                    _listOfDates.Add(data);
                }


            }


            foreach (var date in _listOfDates)
            {
                Console.WriteLine("w dniu" + date + ":");

                foreach (var alert in alertArray)
                {

                    if (alert.TimeOfOccurrence.Date == date)
                    {
                        if (!_dictionaryWithNumberOfAlerts.ContainsKey(alert.TimeOfOccurrence.Hour))
                        {

                            _dictionaryWithNumberOfAlerts.Add(alert.TimeOfOccurrence.Hour, 1);

                        }
                        else
                        {
                            _dictionaryWithNumberOfAlerts[alert.TimeOfOccurrence.Hour] += 1;
                        }

                    }

                }
                foreach (var divace in _dictionaryWithNumberOfAlerts)
                {
                    Console.WriteLine(" godzina: " + divace.Key + ":00" + " ilość alertów " + divace.Value);
                }


            }


        }
    }
}
