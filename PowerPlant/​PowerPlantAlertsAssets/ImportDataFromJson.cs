﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using JsonHelper;

namespace _PowerPlantAlertsAssets
{
   public class ImportDataFromJson
    {
        public AlertOfCrossingDto[] GetData()
        {
            Console.WriteLine("File name:");
            string name = Console.ReadLine();
            if (!name.EndsWith(".json"))
            {
                name = name + ".json";
            }
            string filepath = @"..\..\..\PowerPlandCLI\bin\Debug\" + name;
           // @"C:\Users\Student13\Desktop\etap 2\powerplantewakinga\PowerPlant\PowerPlandCLI\bin\Debug\" + name;
            var jsonMapper=new JsonMapper();
            var assertList = jsonMapper.ToContent<AlertOfCrossingDto[]>(filepath);

            return assertList;
        }
    }
}
