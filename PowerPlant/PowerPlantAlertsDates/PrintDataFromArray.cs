﻿using System;
using System.Collections.Generic;
using System.Linq;
using JsonHelper;

namespace PowerPlantAlertsDates
{
    public class PrintDataFromArray
    {
        private List<string> _deviceList=new List<string>();
        private List<string> _parameterList=new List<string>();

        public void PrintData(AlertOfCrossingDto[] alertArray)
        {
            var fromPeriod = alertArray.Select(a => a.TimeOfOccurrence).Min();
            var toPeriod = alertArray.Select(a => a.TimeOfOccurrence).Max();

            Console.WriteLine("Data range: " + fromPeriod + " - " + toPeriod + "\n");

            foreach (var alert in alertArray)
            {
                if(!_deviceList.Contains(alert.DeviceName))
                    _deviceList.Add(alert.DeviceName);

                if (!_parameterList.Contains(alert.ParameterName))
                    _parameterList.Add(alert.ParameterName);
            }

            foreach (var device in _deviceList)
            {
                Console.WriteLine("Results for: "+device);
                foreach (var parameter in _parameterList)
                {
                  var alertsCount = alertArray.Where(a => a.DeviceName == device).Count(b => b.ParameterName == parameter);
                    if (alertsCount != 0)
                    {
                        Console.WriteLine("\t"+parameter + ": sum of alerts "+alertsCount);
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
