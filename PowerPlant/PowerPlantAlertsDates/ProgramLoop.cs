﻿namespace PowerPlantAlertsDates
{
  public class ProgramLoop
    {
        public void Execute()
        {
            var import=new ImportDataFromJson();
            var array= import.GetData();
            if (array != null)
            {
                var print = new PrintDataFromArray();
                print.PrintData(array);
            }          
        }
    }
}
