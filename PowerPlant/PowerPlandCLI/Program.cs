﻿
using Ninject;
using PowerPlant.NinjectModule;
using PowerPlantCLI.NinjectModule;
using PowerPlantDataLayer.NinjectModule;

namespace PowerPlantCLI
{
    internal class Program
    {
        private static void Main()
        {
            IKernel kernel = new StandardKernel(new ServiceModule(), new RepositoryModule(), new ProgramModule());

            kernel.Get<ProgramLoop>().Execute();
        }
    }
}
