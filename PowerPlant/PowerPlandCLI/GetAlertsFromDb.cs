﻿using System;
using System.IO;
using Newtonsoft.Json;
using PowerPlant.Interfaces;
using PowerPlant.Services;
using PowerPlantCLI.IoHelpers;

namespace PowerPlantCLI
{
   public class GetAlertsFromDb : IGetAlertsFromDb
   {
       private IAlertService _alertService;
       private IGetDateUsingRegex _getDateUsingRegex;
       public GetAlertsFromDb(IAlertService alertService, IGetDateUsingRegex getDateUsingRegex)
       {
           _alertService = alertService;
           _getDateUsingRegex = getDateUsingRegex;
       }
        public void GetAllerts()
        {
            DateTime fromDate = DateTime.Now;
            DateTime toDate;
          
            fromDate= _getDateUsingRegex.GetDateFromRegex("Period from (pattern: 2017/08/25 12:30) ");
            toDate = _getDateUsingRegex.GetDateFromRegex("Period to (pattern: 2017/08/25 12:30) ");
            Console.WriteLine("File name:");
            string fileName = ConsoleReadHelper.GetString("File name: ");
            if (!fileName.EndsWith(".json"))
            {
                fileName = fileName + ".json";
            }
            var alertsList = _alertService.GetPeriodData(fromDate, toDate);

            var jsonMapper = JsonConvert.SerializeObject(alertsList, Formatting.Indented);
            File.WriteAllText(fileName, jsonMapper);
        }     
    }
}
