﻿using System;
using System.Reflection;
using System.Timers;
using JsonHelper;
using PowerPlant.Dtos;
using PowerPlant.Interfaces;
using PowerPlant.JsonFile;
using PowerPlant.Services;
using PowerPlantDataProvider;

namespace PowerPlantCLI
{
   public class GetPowerPlantData : IGetPowerPlantData
   {
       private IGetSettingsDataFromJsonFile _getSettingsDataFromJsonFile;
       private IPowerPlantService _powerPlantService;
       private IEnergyCalculator _energyCalculator;
     
        public GetPowerPlantData(IGetSettingsDataFromJsonFile getSettingsDataFromJsonFile, IPowerPlantService powerPlantService,
            IEnergyCalculator energyCalculator)
       {
           _getSettingsDataFromJsonFile = getSettingsDataFromJsonFile;
           _powerPlantService = powerPlantService;
           _energyCalculator = energyCalculator;
       }
       
        public void LoadData(UserDto userDto)
        {       
            var timer = new Timer();
            timer.Interval = _getSettingsDataFromJsonFile.GetRefreshingTime();
            timer.Elapsed += PrintPowerPlantData;
            timer.Start();
        
            while (Console.ReadKey(true).Key != ConsoleKey.Escape)
            {
                // do nothing until escape
            }
            timer.Stop();
          
        }
        public void LoadTotalData()
        {
            var timer = new Timer();
            timer.Interval = _getSettingsDataFromJsonFile.GetRefreshingTime(); ;
            timer.Elapsed += PrintAdittionPowerPlantData;
            timer.Start();
            while (Console.ReadKey(true).Key != ConsoleKey.Escape)
            {
            }
            timer.Stop();
        }

        private void GetPropertiesValue<T>(T obj)
        {
            foreach (var field in obj.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)) // tutaj albo instance albo static
            {
                var assetParameter = field.GetValue(obj) as AssetParameter;
                if (assetParameter != null)
                {
                    Console.Write(field.Name + " ");
                    CheckIfIsBigger(assetParameter.CurrentValue, assetParameter.MinValue, assetParameter.MaxValue, assetParameter.TypicalValue);
                    Console.WriteLine(assetParameter.CurrentValue+ " " + assetParameter.Unit);
                }
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
        private void PrintPowerPlantData(Object sender, EventArgs e)
        {
            Console.Clear();
            Console.WriteLine(_powerPlantService.GetPowerPlant().Name);

            foreach (var camber in _powerPlantService.GetPowerPlant().Cauldrons)
            {
                Console.WriteLine("\n" + camber.Name + "\n");
                GetPropertiesValue(camber);
            }
            foreach (var turbine in _powerPlantService.GetPowerPlant().Turbines)
            {
                Console.WriteLine("\n" + turbine.Name + "\n");
                GetPropertiesValue(turbine);
            }

            foreach (var transformators in _powerPlantService.GetPowerPlant().Transformators)
            {
                Console.WriteLine("\n" + transformators.Name + "\n");
                GetPropertiesValue(transformators);
            }
        }
        public void PrintAdittionPowerPlantData(Object sender, EventArgs e)
        {
            Console.Clear();
            foreach (var turbine in _powerPlantService.GetPowerPlant().Turbines)
            {
                Console.WriteLine("\n" + turbine.Name + " " + turbine.CurrentPower.CurrentValue + " " + turbine.CurrentPower.Unit);
                _energyCalculator.CalculateEnergySum(turbine.CurrentPower.CurrentValue, _getSettingsDataFromJsonFile.GetRefreshingTime());
            }
            Console.WriteLine("\nTotal energy: " + _energyCalculator.GetSumOfEnergy() + " MWh");
        }
        public void CheckIfIsBigger(double currentValue, double minValue, double maxValue, double typicalValue)
        {
            if (ImportExportJsonFile.SettingsFileData != null)
            {
                minValue = _getSettingsDataFromJsonFile.GetMinValue(typicalValue);
                maxValue = _getSettingsDataFromJsonFile.GetMaxValue(typicalValue);
            }
            if (currentValue < minValue || currentValue > maxValue)
            {
                Console.ForegroundColor = ConsoleColor.Red;
            }
        }  
    }
}
