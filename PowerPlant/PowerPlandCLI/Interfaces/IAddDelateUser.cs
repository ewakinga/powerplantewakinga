namespace PowerPlantCLI
{
    public interface IAddDelateUser
    {
        void AddIfAdminNotExist();
        void AddUser();
        void RemoveUser(int adminId);
    }
}