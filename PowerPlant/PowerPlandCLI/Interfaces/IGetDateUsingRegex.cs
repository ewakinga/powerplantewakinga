using System;

namespace PowerPlantCLI
{
    public interface IGetDateUsingRegex
    {
        DateTime GetDateFromRegex(string message);
    }
}