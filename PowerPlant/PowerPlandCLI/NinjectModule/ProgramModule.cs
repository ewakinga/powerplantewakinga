﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PowerPlantCLI.JsonFile;

namespace PowerPlantCLI.NinjectModule
{
   public class ProgramModule:Ninject.Modules.NinjectModule
    {
        public override void Load()
        {
              Bind<IAddDelateUser>().To<AddDelateUser>();
              Bind<IGeneratePowerPlantCalculation>().To<GeneratePowerPlantCalculation>();
              Bind<IGetAlertsFromDb>().To<GetAlertsFromDb>();
              Bind<IGetDateUsingRegex>().To<GetDateUsingRegex>();
              Bind<IGetPowerPlantData>().To<GetPowerPlantData>();
              Bind<ILogin>().To<Login>();
              Bind<IAdminDataSettings>().To<AdminDataSettings>();

        }
    }
}
