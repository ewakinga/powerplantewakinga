﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Timers;
using JsonHelper;
using PowerPlant.Interfaces;
using PowerPlant.JsonFile;
using PowerPlant.Services;
using PowerPlantDataProvider;

namespace PowerPlantCLI
{
   public class GeneratePowerPlantCalculation : IGeneratePowerPlantCalculation
   {
       private IGetSettingsDataFromJsonFile _getSettingsDataFromJsonFile;
       private IPowerPlantService _powerPlantService;
       private IAlertService _alertService;
      
       public GeneratePowerPlantCalculation(IGetSettingsDataFromJsonFile getSettingsDataFromJsonFile, IPowerPlantService powerPlantService, IAlertService alertService)
       {
           _getSettingsDataFromJsonFile = getSettingsDataFromJsonFile;
           _powerPlantService = powerPlantService;
           _alertService = alertService;
       }
        public static Dictionary<string, bool> WasParameterInBoundaries=new Dictionary<string, bool>();

        Timer _timer = new Timer();
        public void LoadData()
        {     
            _timer.Interval = _getSettingsDataFromJsonFile.GetRefreshingTime();
            _timer.Elapsed += GenerateData;
            _timer.Start();     
        }

        private void GetPropertiesValue<T>(T obj, string name)
        {
            foreach (var field in obj.GetType().GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance)) // tutaj albo instance albo static
            {             
                var assetParameter = field.GetValue(obj) as AssetParameter;
               
                if (assetParameter != null)
                {
                    CheckIfIsBigger(assetParameter.CurrentValue, assetParameter.MinValue, assetParameter.MaxValue,
                        assetParameter.TypicalValue, field, name );              
                }             
            }
            
        }
        private void GenerateData(Object sender, EventArgs e)
        {
            foreach (var camber in _powerPlantService.GetPowerPlant().Cauldrons)
            {
                GetPropertiesValue(camber, camber.Name);
            }

            foreach (var turbine in _powerPlantService.GetPowerPlant().Turbines)
            {
                GetPropertiesValue(turbine, turbine.Name);
            }

            foreach (var transformators in _powerPlantService.GetPowerPlant().Transformators)
            {
                GetPropertiesValue(transformators, transformators.Name);
            }
        }
        public void CheckIfIsBigger(double currentValue, double minValue, double maxValue, double typicalValue,
            PropertyInfo propertyInfo, string name)
        {
            var objectToSave = new AlertOfCrossingDto();

            if (ImportExportJsonFile.SettingsFileData != null)
            {
                minValue =_getSettingsDataFromJsonFile.GetMinValue(typicalValue);
                maxValue = _getSettingsDataFromJsonFile.GetMaxValue(typicalValue);              
            }

            if (currentValue < minValue || currentValue > maxValue)
            {
                if (!WasParameterInBoundaries.ContainsKey(propertyInfo.Name) || WasParameterInBoundaries[propertyInfo.Name])
                {
                    objectToSave.DeviceName = name;
                    objectToSave.ParameterName = propertyInfo.Name;
                    objectToSave.TimeOfOccurrence = DateTime.Now;

                    objectToSave.LoggedUser = Login.User?.Login != null ? Login.User.Login : "N/A";

                    _alertService.AddAlert(objectToSave);

                    if (!WasParameterInBoundaries.ContainsKey(propertyInfo.Name))
                    {
                        WasParameterInBoundaries.Add(propertyInfo.Name, false);
                    }
                    else
                    {
                        WasParameterInBoundaries[propertyInfo.Name] = false;
                    }
                }
            }
            else
            {
                if (WasParameterInBoundaries.ContainsKey(propertyInfo.Name))
                {
                    WasParameterInBoundaries[propertyInfo.Name] = true;
                }
                else
                {
                    WasParameterInBoundaries.Add(propertyInfo.Name, true);
                }
            }
        }
    }
}

