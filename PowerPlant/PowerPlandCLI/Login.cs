﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PowerPlant.Dtos;
using PowerPlant.Interfaces;
using PowerPlant.Services;
using PowerPlantCLI.IoHelpers;

namespace PowerPlantCLI
{
    public class Login : ILogin
    {
        private IUserService _userService;
        private IAddDelateUser _addDelateUser;
        public Login(IUserService userService, IAddDelateUser addDelateUser)
        {
            _userService = userService;
            _addDelateUser = addDelateUser;
        }
        public static UserDto User;

        public bool CheckIfUserIsAdmin(int userId)
        {
            bool admin=true;
            var userList = _userService.GetAllUsers();
            foreach (var user in userList)
            {
                if (user.Id == userId)
                {
                    admin = user.IsAdmin;
                }
            }
            return admin;
        }
        public UserDto Log()
        {
            int id = 0;
           _addDelateUser.AddIfAdminNotExist();
            var userDto = new UserDto();
            bool work = true;

            while (work)
            {
                string tekst = "Get Login";
                Console.WriteLine(tekst);
                userDto.Login = ConsoleReadHelper.GetString(tekst);
                foreach (var user in _userService.GetAllUsers())
                {

                    if (user.Login == userDto.Login)
                    {
                        userDto.Id = user.Id;
                        bool work2 = true;
                        while (work2)
                        {
                            string msg = "Get password";
                            Console.WriteLine(msg);
                            userDto.Password = ConsoleReadHelper.GetString(msg);

                            if (user.Password != userDto.Password)
                            {
                                Console.WriteLine("Wrong password!");
                            }
                            else work2 = false;
                        }

                        work = false;
                    }
                }               
            }
            User = userDto;
            return userDto;
        }
    }
}
