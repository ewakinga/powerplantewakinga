﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PowerPlantDataLayer.DbContext;
using PowerPlantDataLayer.Models;

namespace PowerPlantDataLayer.Repositories
{
   public class AlertRepository : IAlertRepository
   {
        public bool AddAlert(AlertOfCrossing alert)
        {
            var rowsAffected = 0;
            using (var dbContext = new PowerPlantDbContext())
            {
                dbContext.AlertOfCrossingDbSet.Add(alert);
                rowsAffected = dbContext.SaveChanges();
            }
            return (rowsAffected == 1);
        }

        public List<AlertOfCrossing> GetPeriodDataFromDb(DateTime from, DateTime to)
        {
            List<AlertOfCrossing> alertsList;
            List<AlertOfCrossing> alertOfCrossingsList=new List<AlertOfCrossing>();


            using (var dbContext = new PowerPlantDbContext())
            {
                alertsList = dbContext.AlertOfCrossingDbSet.ToList();

                 alertOfCrossingsList = alertsList.Where(a => a.TimeOfOccurrence > from && a.TimeOfOccurrence < to).ToList();
            }
            return alertOfCrossingsList;
        }
    }
}
